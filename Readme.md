# Devfile Example

This is an example to showcase GitLab server runtime. This repository showcases the ability of the server runtime to create environments based on [Devfiles](https://devfile.io). We can start a new environment by clicking [here](https://glide.shekharpatnaik.co.uk?project=40607001&ref=main)
